#ifndef FNN_H__
#define FNN_H__

#include <string>

#include "nn_structures.h"

namespace nn
{
	struct Topology
	{
		Topology(const std::vector<int>&& topology)
			: topology_(topology), num_layers_(static_cast<int>(topology.size() - 1)) {}

		int num_layers_;

		std::vector<int> topology_;
	};

	class FNN
	{
	public:
		FNN(const Topology&& topology, double learning_rate);
		~FNN() = default;

		void Backprop();
		double BackpropError() const 
		{ return error_; }

		void FeedForward();

		void Save(const std::string&& path);
		void SetInputVector(const nn::vec1d<double>& inputs);
		void SetTargetVector(const nn::vec1d<double>&& targets);

		std::vector<double> Outputs() const { return activations_.back(); }
		std::vector<double> Targets() const { return targets_; }

	private:
		int input_layer_size_ = 0, number_layers_ = 0;
		double learning_rate_ = 0.2, error_ = 0.0;

		nn::vec1d<double> inputs_;
		nn::vec1d<double> targets_;

		nn::vec2d<double> activations_; //[layer x neuron_activations_in_current_layer]
		nn::vec2d<double> logits_;

		nn::vec3d<double> weights_; //[layer x num_neurons_in_next_layer x num_neurons_in_prev_layer]
		nn::vec3d<double> weight_deltas_;

		nn::vec2d<double> biases_; //[layer x number of neurons] 
	};
}	//	namespace nn
#endif
