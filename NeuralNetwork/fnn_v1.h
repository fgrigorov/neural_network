#ifndef FNN_V1_H
#define FNN_V1_H

#include <map>

#include "nn_structures.h"

namespace nn
{
	std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>> BatchData(const std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>>& data, int num_batches);

	std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>> Shuffle(const nn::vec2d<double>& x_samples, const nn::vec2d<double>& y_samples);

	class ForwardNeuralNet
	{
	public:
		ForwardNeuralNet(const std::vector<int>& topology, double learning_rate);
		~ForwardNeuralNet() = default;

		ForwardNeuralNet(const ForwardNeuralNet& obj) = delete;
		ForwardNeuralNet& operator=(const ForwardNeuralNet& obj) = delete;

		void Backward(std::map<std::string, nn::vec1d<double>> output);

		std::map<std::string, nn::vec1d<double>> Forward(const vec1d<double>& X, const vec1d<double>& y_true);

	private:
		int num_layers_ = 0;
		double lr_;

		vec2d<double> activations_, biases_, bias_gradients_, logits_, delta_errors_;
		vec3d<double> weights_, weight_gradients_;
	};
}  // namespace nn
#endif