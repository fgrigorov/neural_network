#pragma once
#include <cassert>
#include <cmath>
#include <random>

#include "nn_structures.h"

/* lfs versioning of files with given extension
*	.jpg filter=lfs diff=lfs merge=lfs -text
*	.plan filter=lfs diff=lfs merge=lfs -text
*/

namespace math
{
	nn::vec1d<double> MSELoss(const nn::vec1d<double>& targets, const nn::vec1d<double>& outputs)
	{
		assert(targets.size() == outputs.size());

		nn::vec1d<double> errors;
		for (size_t idx = 0; idx < targets.size(); ++idx)
		{
			errors.push_back(0.5 * (targets.at(idx) - outputs.at(idx)) * (targets.at(idx) - outputs.at(idx)));
		}

		return errors;
	}

	nn::vec1d<double> Add(const nn::vec1d<double>& rhs, const nn::vec1d<double>& lhs)
	{
		assert(rhs.size() == lhs.size());

		nn::vec1d<double> result;
		for (int idx = 0; idx < rhs.size(); ++idx)
		{
			result.push_back(rhs[idx] + lhs[idx]);
		}

		return result;
	}

	nn::vec2d<double> Add(const nn::vec2d<double>& rhs, const nn::vec2d<double>& lhs)
	{
		assert(rhs.size() == lhs.size());
		assert(rhs.front().size() == lhs.front().size());

		nn::vec2d<double> result;
		for (int l = 0; l < rhs.size(); ++l)
		{
			nn::vec1d<double> row;
			for (int idx = 0; idx < rhs.at(l).size(); ++idx)
			{
				row.push_back(rhs.at(l).at(idx) + lhs.at(l).at(idx));
			}
			result.push_back(row);
		}

		return result;
	}

	double MSEDerivative(double target, double prediction)
	{
		return prediction - target; // (a_j - y_j)
	}

	nn::vec1d<double> MSEDerivative(const nn::vec1d<double>& targets, const nn::vec1d<double>& predictions)
	{
		auto targets_size = targets.size();
		auto predictions_size = predictions.size();
		assert(targets_size == predictions_size);

		nn::vec1d<double> result;
		for (int idx = 0; idx < targets_size; ++idx)
		{
			result.push_back(MSEDerivative(targets.at(idx), predictions.at(idx)));
		}

		return result;
	}

	//Activation functions
	double Sigmoid(double value)
	{
		//Note: std::exp2 is the same as 2 ^ x
		return 1.0 / (1.0 + std::exp(-value));
	}

	nn::vec2d<double> Sigmoid(const nn::vec2d<double>& vec2d)
	{
		nn::vec2d<double> result;
		for (int l = 0; l < vec2d.size(); ++l)
		{
			nn::vec1d<double> row;
			for (int idx = 0; idx < vec2d.at(l).size(); ++idx)
			{
				row.push_back(Sigmoid(vec2d.at(l).at(idx)));
			}
			result.push_back(row);
		}

		return result;
	}

	double DerivativeOfSigmoid(double value)
	{
		return Sigmoid(value) * (1.0 - Sigmoid(value));
	}

	nn::vec2d<double> DerivativeOfSigmoid(const nn::vec2d<double>& vec2d)
	{
		nn::vec2d<double> result;
		for (int l = 0; l < vec2d.size(); ++l)
		{
			nn::vec1d<double> row;
			for (int idx = 0; idx < vec2d[l].size(); ++idx)
			{
				row.push_back(DerivativeOfSigmoid(vec2d[l][idx]));
			}
			result.push_back(row);
		}

		return result;
	}

	double GenerateRandom(const std::pair<double, double>&& range)
	{
		std::random_device rd;
		std::mt19937_64 engine(rd());
		std::uniform_real_distribution<> dist(range.first, range.second);

		return dist(engine);
	}

	template <typename T_, typename = std::enable_if_t<std::is_arithmetic<T_>::value> >
	nn::vec2d<T_> matmul(const nn::vec2d<T_>& vec_one, const nn::vec2d<T_>& vec_two) {
		auto rows_one = vec_one.size();
		auto cols_one = vec_one.front().size();
		auto rows_two = vec_two.size();
		auto cols_two = vec_two.front().size();

		if (cols_one != rows_two) { throw std::runtime_error("math::error: cols (m1) != rows (m2)!"); }

		nn::vec2d<T_> product(rows_one, nn::vec1d<T_>(cols_two, static_cast<T_>(0.0)));
		for (size_t row_one = 0; row_one < rows_one; ++row_one) {
			for (size_t col_one = 0; col_one < cols_one; ++col_one) {
				for (size_t col_two = 0; col_two < cols_two; ++col_two) {
					product[row_one][col_two] += vec_one[row_one][col_one] * vec_two[col_one][col_two];
				}
			}
		}
		return std::move(product);
	}

	template <typename T_, typename = std::enable_if_t<std::is_arithmetic<T_>::value> >
	nn::vec2d<T_> hadamard(const nn::vec2d<T_>& vec_one, const nn::vec2d<T_>& vec_two) {
		auto rows_one = vec_one.size();
		auto cols_one = vec_one.front().size();
		auto rows_two = vec_two.size();
		auto cols_two = vec_two.front().size();

		if (rows_one != rows_two && cols_one != cols_two) { throw std::runtime_error("math::eror: Sizes of matrices are not equal!"); }

		nn::vec2d<T_> product(rows_one, nn::vec1d<T_>(cols_one, static_cast<T_>(0.0)));
		for (size_t row = 0; row < rows_one; ++row) {
			for (size_t col = 0; col < cols_one; ++col) {
				product[row][col] = vec_one[row][col] * vec_two[row][col];
			}
		}
		return std::move(product);
	}

	template <typename T_, typename = std::enable_if_t<std::is_arithmetic<T_>::value> >
	nn::vec2d<T_> transpose(const nn::vec2d<T_>& vec) {
		auto rows = vec.size();
		auto cols = vec.front().size();

		nn::vec2d<T_> transposed(cols, nn::vec1d<T_>(rows, static_cast<T_>(0.0)));
		for (size_t col = 0; col < cols; ++col) {
			for (size_t row = 0; row < rows; ++row) {
				transposed[col][row] = vec[row][col];
			}
		}
		return std::move(transposed);
	}

	nn::vec1d<double> ForwardPass(const nn::vec3d<double>& weights, const nn::vec2d<double>& biases, const nn::vec2d<double>& activations)
	{
		auto num_layers = static_cast<int>(activations.size());
		nn::vec3d<double> w = weights;
		nn::vec2d<double> b = biases;
		nn::vec2d<double> a = activations;

		for (int l = 1; l < num_layers; ++l)
		{
			int cols = static_cast<int>(weights[l - 1].front().size());
			for (int col = 0; col < cols; ++col)
			{
				int rows = static_cast<int>(weights[l - 1].size());
				for (int row = 0; row < rows; ++row)
				{
					a[l][col] += w[l - 1][row][col] * a[l - 1][row];
				}
				a[l][col] += b[l][col];
				a[l][col] = Sigmoid(a[l][col]);
			}
		}

		return std::move(a.back());
	}

	//https://medium.com/@karpathy/yes-you-should-understand-backprop-e2f06eab496b
	template <typename F_>
	nn::vec3d<double> ComputeNumericalGradients(F_ *cost,
									const nn::vec1d<double>& targets,
									const nn::vec3d<double>& weights,
									const nn::vec2d<double>& biases,
									const nn::vec2d<double>& activations)
	{
		const double epsilon = 0.0000001;
		const double twice_epsilon = 2 * epsilon;

		auto num_layers = static_cast<int>(activations.size());

		nn::vec3d<double> grads = weights;

		//Note: For each weight at each layer of the grad matrix we are computing
		for (int l = 0; l < num_layers - 1; ++l)
		{
			auto rows = static_cast<int>(weights[l].size());
			for (int row = 0; row < rows; ++row)
			{
				auto cols = static_cast<int>(weights[l][row].size());
				for (int col = 0; col < cols; ++col)
				{
					//Note: Start forward pass from here for each weight change
					nn::vec3d<double> weights_plus = weights;
					weights_plus[l][row][col] += epsilon;
					nn::vec1d<double> predictions_plus = ForwardPass(weights_plus, biases, activations);
					nn::vec1d<double> errors_plus = (*cost)(targets, predictions_plus);

					nn::vec3d<double> weights_minus = weights;
					weights_minus[l][row][col] -= epsilon;
					nn::vec1d<double> predictions_minus = ForwardPass(weights_minus, biases, activations);
					nn::vec1d<double> errors_minus = (*cost)(targets, predictions_minus);

					auto size_plus = static_cast<int>(errors_plus.size());
					auto size_minus = static_cast<int>(errors_minus.size());

					assert(size_plus == size_minus);

					double gradient = 0.0;
					for (int idx = 0; idx < size_plus; ++idx)
					{
						gradient += errors_plus[idx] - errors_minus[idx];
					}

					gradient /= size_plus;
					gradient /= twice_epsilon;

					grads[l][row][col] = gradient;
				}
			}
		}

		return grads;
	}
}  // namespace
