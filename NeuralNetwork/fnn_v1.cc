//https://en.cppreference.com/w/cpp/language/object#Alignment
//http://ufldl.stanford.edu/wiki/index.php/UFLDL_Tutorial
//TODO(Filip): switch to smaller data type like float (32bits instead of 64 bits double)
//TODO(Filip): optimize
#include "fnn_v1.h"

#include <algorithm>
#include <cassert>
#include <vector>

#include "math_ops.h"

std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>> nn::BatchData(const std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>>& data, int num_batches)
{
	auto data_size = data.size();
	auto batch_stride = data_size / num_batches;

	std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>> batched_data;
	for (int idx = 0; idx < ((data_size % 2 == 0) ? data_size : data_size - 1); idx += num_batches)
	{
		batched_data.push_back(data.at(idx));
	}

	if (data_size % 2 != 0)
	{
		auto begin_itr = batched_data.begin();
		auto end_itr = batched_data.begin() + batch_stride - 1;
		std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>> subvector(begin_itr, end_itr);
		subvector.push_back(batched_data.back());
		batched_data.insert(batched_data.end(), subvector.begin(), subvector.end());
	}

	return batched_data;
}

std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>> > nn::Shuffle(const nn::vec2d<double>& x_samples, const nn::vec2d<double>& y_samples)
{
	auto x_size = x_samples.size();
	auto y_size = y_samples.size();
	assert(x_size == y_size);

	std::vector<int> indices;
	for (int idx = 0; idx < x_size; ++idx)
	{
		indices.push_back(idx + 1);
	}

	std::random_shuffle(std::begin(indices), std::end(indices));

	std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>> data;

	for (int idx = 0; idx < indices.size(); ++idx) {
		data.emplace_back(x_samples.at(indices.at(idx) - 1), y_samples.at(indices.at(idx) - 1));
	}

	return std::move(data);
}

namespace nn {
	ForwardNeuralNet::ForwardNeuralNet(const std::vector<int>& topology, double learning_rate)
		: lr_(learning_rate)
	{
		num_layers_ = static_cast<int>(topology.size());
		for (int l = 0; l < num_layers_; ++l) 
		{
			logits_.push_back(std::vector<double>(topology[l], 0.0));
			activations_.push_back(std::vector<double>(topology[l], 0.0));
			biases_.push_back(std::vector<double>(topology[l], 0.5));						//TODO(Filip): better initialization
			bias_gradients_.push_back(std::vector<double>(topology[l], 0.0));
			delta_errors_.push_back(std::vector<double>(topology[l], 0.0));

			if (l < num_layers_ - 1)
			{
				int rows = topology[l];
				int cols = topology[l + 1];
				vec2d<double> weight_matrix(rows, vec1d<double>(cols, 0.0));

				weights_.push_back(weight_matrix);
				weight_gradients_.push_back(weight_matrix);
			}
		}

		for (int l = 0; l < weights_.size(); ++l)
		{
			for (int row = 0; row < weights_[l].size(); ++row)
			{
				for (int col = 0; col < weights_[l][row].size(); ++col)
				{
					weights_[l][row][col] = math::GenerateRandom(std::make_pair(0.1, 1.0));
				}
			}
		}

#ifdef DEBUG_
		nn::Print3d(weights_, "Weights");
		nn::Print2d(biases_, "Biases");

		nn::Shape3d(weights_, "Weights");
		nn::Shape2d(biases_, "Biases");

		nn::Shape2d(logits_, "Logits");
		nn::Shape2d(activations_, "Activations");
#endif
	}

	void ForwardNeuralNet::Backward(std::map<std::string, nn::vec1d<double>> output)
	{
		nn::vec1d<double> predictions = output["prediction"];
		nn::vec1d<double> targets = output["target"];

		//Output error:
		auto output_errors = math::MSELoss(predictions, targets);

		double total_output_error = 0.0;
		for (const auto& error : output_errors)
		{
			total_output_error += error;
		}
		total_output_error /= output_errors.size();

#ifdef DEBUG_
		std::cout << "Error= " << total_output_error << "\n";
#endif

		/*-------------------------------------------------- Backprop --------------------------------------------------*/
		//Output layer
		nn::vec2d<double> output_d = { math::MSEDerivative(targets, predictions) };
		nn::vec2d<double> output_z = { logits_.back() };
		auto output_activation_derivative = math::DerivativeOfSigmoid(output_z);
#ifdef DEBUG_
		nn::Print2d(output_d, "output_d");
		nn::Shape2d(output_d, "output_d");
#endif
		auto output_delta2d = math::hadamard(output_d, output_activation_derivative);
		nn::vec2d<double> output_prev_a = { activations_.at(activations_.size() - 2) };

		delta_errors_.back() = output_delta2d.front();

		weight_gradients_.back() = math::Add(weight_gradients_.back(), math::matmul(math::transpose(output_prev_a), output_delta2d));
		bias_gradients_.back() = math::Add(bias_gradients_.back(), output_delta2d.front());

		nn::vec2d<double> layer_d = output_delta2d;
		for (int l = num_layers_ - 1; l > 1; --l)
		{
			auto weight_matrix_transposed = math::transpose(weights_[l - 1]);

			nn::vec2d<double> prev_d = { delta_errors_[l] };
			nn::vec2d<double> layer_d = math::matmul(prev_d, weight_matrix_transposed);

			nn::vec2d<double> layer_logit2d = { logits_[l - 1] };
			layer_d = math::hadamard(layer_d, math::DerivativeOfSigmoid(layer_logit2d));

			nn::vec2d<double> layer_prev_activation2d = { activations_[l - 2] };

			delta_errors_[l - 1] = layer_d.front();

			weight_gradients_[l - 2] = math::Add(weight_gradients_[l - 2], math::matmul(math::transpose(layer_prev_activation2d), layer_d));
			bias_gradients_[l - 1] = math::Add(bias_gradients_[l - 1], layer_d.front());
		}

#ifdef DEBUG_
		nn::Print3d(weight_gradients_, "Weight gradients");
		nn::Print2d(bias_gradients_, "Bias gradients");
#endif

#ifdef DEBUG_
		//http://cs231n.github.io/optimization-1/
		auto grads = math::ComputeNumericalGradients(&math::MSELoss, targets, weights_, biases_, activations_);
		nn::Print3d(grads, "Analytic gradient");
#endif
	}

	//Note: a = sigma(x.w + b)
	std::map<std::string, nn::vec1d<double>> ForwardNeuralNet::Forward(const vec1d<double>& X, const vec1d<double>& y_true)
	{
		std::map<std::string, nn::vec1d<double>> output;
		logits_.front() = X;
		activations_.front() = X;

		for (int l = 1; l < num_layers_; ++l)
		{
			nn::vec2d<double> activation_matrix = { activations_[l - 1] };
			nn::vec2d<double> bias2d = { biases_[l] };
			auto logit = math::Add(math::matmul(activation_matrix, weights_[l - 1]), bias2d);
			auto product = math::Sigmoid(logit);
			logits_[l] = logit.front();
			activations_[l] = product.front();
		}

#ifdef DEBUG_
		nn::Print2d(logits_, "Logits (One forward pass)");
		nn::Print2d(activations_, "Activations (One forward pass)");
#endif
		output.emplace("prediction", activations_.back());
		output.emplace("target", y_true);
		return output;
	}
}  // namespace nn