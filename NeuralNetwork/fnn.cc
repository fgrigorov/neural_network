#include "fnn.h"

#include <cassert>
#include <iostream>
#include <random>

//SSE2 optimizations
#include <emmintrin.h>

#define TMP

//#define DEBUG_
#ifdef DEBUG_
#pragma message("Note: Debugging printing is enabled!")
#endif

#define GRADIENT_CHECKING_
#ifdef GRADIENT_CHECKING_
#pragma message("Note: Gradient checking is enabled!")
#endif

namespace 
{
	const double kEpsilon = 1e-8;

	double GenerateRandom(const std::pair<double, double>&& range);

	bool IsAtHiddenLayer(int layer) { return layer > 1; }

	nn::vec1d<double> LossFunctionDerivative(const nn::vec1d<double>& targets, const nn::vec1d<double>& outputs);

	double MSEDerivative(double target, double output);
	double MSELoss(const nn::vec1d<double>& targets, const nn::vec1d<double>& outputs);

	//Activation functions
	double LeakyReLu(double value);
	double ReLu(double value);
	double Sigmoid(double value);

	//Derivatives of activation functions
	double DerivativeOfSigmoid(double value);

	nn::vec2d<double> GradientCheckForMSE(const nn::vec2d<double>& weights, 
		const nn::vec1d<double>& biases, 
		const nn::vec1d<double>& targets);
}	//	namespace

nn::FNN::FNN(const nn::Topology&& topology, double learning_rate)
	:	input_layer_size_(topology.topology_.front()), 
		number_layers_(static_cast<int>(topology.topology_.size() - 1)), 
		learning_rate_(learning_rate)
{
	//Note: add inputs as the first set of activations
	auto activation_vector = nn::vec1d<double>(topology.topology_.front(), 0.0);
	activations_.push_back(activation_vector);
	logits_.push_back(activation_vector);

	//Note: add next set of hidden activations and output activation
	for (size_t layer = 1; layer < topology.topology_.size(); ++layer) 
	{
		auto activation_vector = nn::vec1d<double>(topology.topology_.at(layer), 0.0);
		activations_.push_back(activation_vector);
		logits_.push_back(activation_vector);

		auto bias_vector = nn::vec1d<double>(topology.topology_.at(layer), 0.5);
		biases_.push_back(bias_vector);
		
		nn::vec2d<double> rows_of_weights_matrix, row_of_weight_deltas_matrix;
		for (auto row = 0; row < topology.topology_.at(layer); ++row)
		{
			nn::vec1d<double> column_of_weights_matrix, column_of_weight_deltas_matrix;
			//auto column =nn::vec1d<double>(topology.topology_.at(layer - 1), 0.0);
			for (auto col = 0; col < topology.topology_.at(layer - 1); ++col)
			{
				column_of_weights_matrix.push_back(GenerateRandom({ -1.0, 1.0 }));
				column_of_weight_deltas_matrix.push_back(0.0);
			}
			rows_of_weights_matrix.push_back(column_of_weights_matrix);
			row_of_weight_deltas_matrix.push_back(column_of_weight_deltas_matrix);
		}
		weights_.push_back(rows_of_weights_matrix);
		weight_deltas_.push_back(row_of_weight_deltas_matrix);
	}

#ifdef DEBUG_
	/*Print layers for debugging*/
	std::cout << "\nInitial activations vector: \n";
	for (size_t layer = 0; layer < activations_.size(); ++layer) {
		for (size_t neuron = 0; neuron < activations_.at(layer).size(); ++neuron) {
			std::cout << " " << activations_.at(layer).at(neuron) << " ";
		}
		std::cout << "\n";
	}

	/*Print weights for debugging*/
	std::cout << "\nWeights vector: \n";
	for (size_t layer = 0; layer < weights_.size(); ++layer) {
		for (size_t row = 0; row < weights_.at(layer).size(); ++row) {
			std::cout << "\n";
			for (size_t col = 0; col < weights_.at(layer).at(row).size(); ++col) {
				std::cout << " " << weights_.at(layer).at(row).at(col) << " ";
			}
		}
		std::cout << "\n\n";
	}

	/*Print outputs*/
	std::cout << "output layer: \n";
	for (size_t neuron = 0; neuron < activations_.back().size(); ++neuron)
	{
		std::cout << " " << activations_.back().at(neuron) << " ";
	}

	std::cout << "\n---------------------------------------------------------------------------------\n";
#endif
}

void nn::FNN::Backprop()
{
	assert(targets_.size() == activations_.back().size());
#ifdef DEBUG_
	std::cout << "output layer: \n";
	for (size_t neuron = 0; neuron < activations_.back().size(); ++neuron) {
		std::cout << " " << activations_.back().at(neuron) << " ";
	}
#endif

	error_ = MSELoss(targets_, activations_.back());

	auto gradients_checks = GradientCheckForMSE(weights_.back(), 
		biases_.at(biases_.size() - 2), 
		targets_);

	nn::vec2d<double> errors(number_layers_);

	//OUTPUT LAYER
	nn::vec1d<double> output_errors = LossFunctionDerivative(targets_, activations_.back());
	for (size_t neuron = 0; neuron < activations_.back().size(); ++neuron)
	{
		output_errors[neuron] = output_errors.at(neuron);
		output_errors[neuron] *= DerivativeOfSigmoid(logits_.back().at(neuron));
	}

	errors[number_layers_ - 1] = output_errors;


	//HIDDEN LAYERS
	for (size_t layer = number_layers_; layer > 0; --layer)
	{
		nn::vec1d<double> prev_layer_errors;

		std::cout << layer << std::endl;

		auto weights_matrix_of_current_layer = weights_.at(layer - 1);

		auto rows = weights_matrix_of_current_layer.size();
		auto cols = weights_matrix_of_current_layer.front().size();

		nn::vec1d<double> error_terms_per_layer_per_neuron(cols);
		for (size_t col = 0; col < cols; ++col) 
		{
			double error_term_per_prev_neuron = 0.0;
			for (size_t row = 0; row < rows; ++row) 
			{
				auto a = weights_matrix_of_current_layer.at(row).at(col);
				auto b = errors.at(layer - 1).at(row);
				auto c = DerivativeOfSigmoid(logits_.at(layer).at(row));
				//Propagate error terms at next layer to previous layer's neurons: all errors @ j to a neuron i
				error_term_per_prev_neuron += weights_matrix_of_current_layer.at(row).at(col) *
												errors.at(layer - 1).at(row) *
												DerivativeOfSigmoid(logits_.at(layer).at(row));
				//Implementation note:
				//DerivSigmoid = Sigmoid(1 - Sigmoid) but in our case, Sigmoid(z_l) = a_l, so
				//DerivSigmoid = a_l(1 - a_l)
				//Also, add regulzarization terms to the equation

				//Compute the derivatives of Loss wrt weights: dL/dw
				weight_deltas_[layer - 1][row][col] += errors.at(layer - 1).at(row) * activations_.at(layer - 1).at(col);
			}

			if (IsAtHiddenLayer(layer)) { error_terms_per_layer_per_neuron[col] = error_term_per_prev_neuron; }
		}

		if (IsAtHiddenLayer(layer)) {
			errors[layer - 2] = error_terms_per_layer_per_neuron;
		}

/*
		for (size_t row = 0; row < weights_matrix_of_current_layer.size(); ++row)
		{
			for (size_t col = 0; col < weights_matrix_of_current_layer.at(row).size(); ++col)
			{
				auto per_neuron_error_term = weights_matrix_of_current_layer.at(row).at(col) * errors.at(layer - 1).at(row);
				std::cout << "debug-2\n";
				weight_deltas_[layer - 1][row][col] =  errors.at(layer - 1).at(row) * activations_.at(layer - 1).at(col);

				std::cout << "debug-1\n";
				if (IsAtHiddenLayer(layer))
					prev_layer_errors.push_back(weighted_error_term * DerivativeOfSigmoid(logits_.at(layer).at(row)));
			}
		}
		std::cout << "debug0\n";
		if (IsAtHiddenLayer(layer))
			errors[layer - 1] = prev_layer_errors;
*/
	}

	//Update weights: w -= nu * delta_w
	for (size_t mid_layer = 0; mid_layer < weights_.size(); ++mid_layer) {
		for (size_t row = 0; row < weights_.at(mid_layer).size(); ++row) {
			for (size_t col = 0; col < weights_.at(mid_layer).at(row).size(); ++col) {
				weights_[mid_layer][row][col] -= learning_rate_ * weight_deltas_[mid_layer][row][col];
			}
		}
	}

#ifdef GRADIENT_CHECKING_
	/*Print gradients checks*/
	std::cout << "\n\nGradients checks: \n\n";
	for (size_t row = 0; row < gradients_checks.size(); ++row) {
		for (size_t col = 0; col < gradients_checks.at(row).size(); ++col) {
			std::cout << " " << gradients_checks.at(row).at(col) << " VS " << weight_deltas_.back().at(row).at(col) << " \n";
		}
	}
	std::cout << "\n\n";
#endif

#ifdef DEBUG_
	/*Print weights for debugging*/
	std::cout << "\nWeights vector: \n";
	for (size_t layer = 0; layer < weights_.size(); ++layer) {
		for (size_t row = 0; row < weights_.at(layer).size(); ++row) {
			std::cout << "\n";
			for (size_t col = 0; col < weights_.at(layer).at(row).size(); ++col) {
				std::cout << " " << weights_.at(layer).at(row).at(col) << " ";
			}
		}
		std::cout << "\n\n";
	}
#endif
}

void nn::FNN::FeedForward()
{
	for (size_t idx = 0; idx < inputs_.size(); ++idx) 
	{ 
		logits_[0][idx] = inputs_.at(idx);
		activations_[0][idx] = inputs_.at(idx); 
	}

	//Note: we do not include input activations
	assert(weights_.size() == activations_.size() - 1);

	//Iterate through next layers in the network (weights_.size() + 1 in 
	//order to go to the output layer as activations > weights by 1)
	for (size_t layer = 1; layer < weights_.size() + 1; ++layer)
	{
		auto current_layer_weights = weights_.at(layer - 1);
		for (size_t row = 0; row < current_layer_weights.size(); ++row)
		{
			for (size_t col = 0; col < current_layer_weights.at(row).size(); ++col)
			{
				logits_[layer][row] += current_layer_weights.at(row).at(col) * activations_.at(layer - 1).at(col);
			}

			auto logit = ((layer == weights_.size()) ?  logits_[layer][row] : 
														logits_[layer][row] + biases_.at(layer - 1).at(row));
			activations_[layer][row] = Sigmoid(logit);
		}
	}

#ifdef DEBU
	/*Print layers for debugging*/
	std::cout << "\nActivations vector: \n";
	for (size_t layer = 0; layer < activations_.size(); ++layer) {
		for (size_t neuron = 0; neuron < activations_.at(layer).size(); ++neuron) {
			std::cout << " " << activations_.at(layer).at(neuron) << " ";
		}
		std::cout << "\n";
	}
#endif
}

void nn::FNN::Save(const std::string&& path)
{
	
}

void nn::FNN::SetInputVector(const nn::vec1d<double>& inputs)
{
	if (!inputs_.empty()) { inputs_.clear(); }
	std::copy(std::cbegin(inputs), std::cend(inputs), std::back_inserter(inputs_));

	/*Print inputs for debugging*/
	/*
	std::cout << "inputs: { ";
	for (size_t idx = 0; idx < inputs_.size(); ++idx) {
		std::cout << " " << inputs_.at(idx) << " ";
	}
	std::cout << " }\n";
	*/
}

void nn::FNN::SetTargetVector(const nn::vec1d<double>&& targets)
{
	if (!targets_.empty()) { targets_.clear(); }
	std::copy(std::cbegin(targets), std::cend(targets), std::back_inserter(targets_));
}

namespace 
{
	double GenerateRandom(const std::pair<double, double>&& range)
	{
		std::random_device rd;
		std::mt19937_64 engine(rd());
		std::uniform_real_distribution<> dist(range.first, range.second);

		return dist(engine);
	}

	nn::vec1d<double> LossFunctionDerivative(const nn::vec1d<double>& targets, const nn::vec1d<double>& outputs)
	{
		assert(targets.size() == outputs.size());

		nn::vec1d<double> output_errors;
		for (size_t idx = 0; idx < targets.size(); ++idx)
		{
			output_errors.push_back(MSEDerivative(targets.at(idx), outputs.at(idx)));
		}

		return std::move(output_errors);
	}

	double MSEDerivative(double target, double output)
	{
		return output - target; // (a_j - y_j)
	}

	double MSELoss(const nn::vec1d<double>& targets, const nn::vec1d<double>& outputs)
	{
		assert(targets.size() == outputs.size());

		double error = 0.0;
		for (size_t idx = 0; idx < targets.size(); ++idx)
		{
			error += 0.5 * (targets.at(idx) - outputs.at(idx)) * (targets.at(idx) - outputs.at(idx));
		}

		return error / targets.size();
	}

	//Actiation functions
	double LeakyReLu(double value)
	{
		auto slope = 0.01;
		return std::max(value, slope * value);
	}

	double ReLu(double value)
	{
		return std::max(value, 0.0);
	}

	double Sigmoid(double value)
	{
		//Note: std::exp2 is the same as 2 ^ x
		return 1.0 / (1.0 + std::exp(-value));
	}

	//Derivatives of activation functions
	double DerivativeOfSigmoid(double value)
	{
		return Sigmoid(value) * (1.0 - Sigmoid(value));
	}

	nn::vec2d<double> GradientCheckForMSE(const nn::vec2d<double>& weights, 
		const nn::vec1d<double>& biases, 
		const nn::vec1d<double>& targets)
	{
		assert(!weights.empty());

		//Note: two-point formula
		nn::vec2d<double> positive(weights.size());
		nn::vec2d<double> negative(weights.size());
		nn::vec2d<double> gradients(weights.size());
		for (size_t row = 0; row < weights.size(); ++row)
		{
			for (size_t col = 0; col < weights.at(row).size(); ++col)
			{
				double forward_value_pos = weights.at(row).at(col) + kEpsilon;
				double forward_value_neg = weights.at(row).at(col) - kEpsilon;

				forward_value_pos = Sigmoid(forward_value_pos + biases.at(col));
				forward_value_neg = Sigmoid(forward_value_neg + biases.at(col));

				forward_value_pos = 0.5 * (targets.at(row) - forward_value_pos) * (targets.at(row) - forward_value_pos);
				forward_value_neg = 0.5 * (targets.at(row) - forward_value_neg) * (targets.at(row) - forward_value_neg);

				positive[row].push_back(forward_value_pos);
				negative[row].push_back(forward_value_neg);
				gradients[row].push_back((positive.at(row).at(col) - negative.at(row).at(col)) / ( 2 * kEpsilon));
			}
		}

		return gradients;
	}
}	//	namespace
