#include "nn_structures.h"

namespace nn
{
	void Print1d(const vec1d<double>& vec1d, const std::string& title)
	{
		std::cout << "\n" << title << ": \n";
		for (size_t neuron = 0; neuron < vec1d.size(); ++neuron)
		{
			std::cout << " " << vec1d.at(neuron) << " ";
		}
	}

	void Print2d(const vec2d<double>& vec2d, const std::string& title)
	{
		std::cout << "\n" << title << ": \n";
		for (size_t layer = 0; layer < vec2d.size(); ++layer) {
			for (size_t neuron = 0; neuron < vec2d.at(layer).size(); ++neuron) {
				std::cout << " " << vec2d.at(layer).at(neuron) << " ";
			}
			std::cout << "\n";
		}
	}

	void Print3d(const vec3d<double>& vec3d, const std::string& title)
	{
		std::cout << "\n" << title << ": \n";
		for (size_t layer = 0; layer < vec3d.size(); ++layer) {
			for (size_t row = 0; row < vec3d.at(layer).size(); ++row) {
				std::cout << "\n";
				for (size_t col = 0; col < vec3d.at(layer).at(row).size(); ++col) {
					std::cout << " " << vec3d.at(layer).at(row).at(col) << " ";
				}
			}
			std::cout << "\n\n";
		}
	}

	void Shape1d(const vec1d<double>& vec1d, const std::string& title)
	{
		std::cout << "\n" << title << " shape: ";
		std::cout << "{ " << vec1d.size() << " }\n";
	}

	void Shape2d(const vec2d<double>& vec2d, const std::string& title)
	{
		std::cout << "\n" << title << " shape: ";
		std::cout << "[" << vec2d.size() << ", " << vec2d.front().size() << "]\n";
	}

	void Shape3d(const vec3d<double>& vec3d, const std::string& title)
	{
		std::cout << "\n" << title << " shape: ";
		for (int l = 0; l < vec3d.size(); ++l)
		{
			std::cout << " [" << vec3d.at(l).size() << ", " << vec3d.at(l).front().size() << "] ";
		}
		std::cout << "\n";
	}
}	//	namespace nn