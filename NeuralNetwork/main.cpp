#include <cassert>
#include <chrono>
#include <iostream>
#include <memory>
#include <vector>

//TODO(Filip): Add regularization
//TODO(Filip): Add convolution

//#include "fnn.h"
#include "fnn_v1.h"

#define NUM_BATCHES 10

//int main_v1(void);
int main_v2(void);

int main(int argc, char** argv)
{
	return main_v2();
}

//Goal: mnist classification, regression
int main_v2(void)
{
	const int epochs = 1;
	//TODO: adaptive learning rate
	double learning_rate = 3e-3;
	std::vector<int> topology = { 2, 3, 1 };
	auto net = std::make_shared<nn::ForwardNeuralNet>(topology, learning_rate);

	nn::vec2d<double> x_samples = {	{0.0, 0.0}, 
									{1.0, 0.0}, 
									{0.0, 1.0}, 
									{1.0, 1.0} };
	nn::vec2d<double> y_samples = {	{0.0}, 
									{0.0}, 
									{0.0}, 
									{1.0} };
	std::vector<std::pair<nn::vec1d<double>, nn::vec1d<double>>> shuffled_data = nn::Shuffle(x_samples, y_samples);
	auto size_of_data = shuffled_data.size();
	const int num_batches = static_cast<const int>((size_of_data < NUM_BATCHES) ? size_of_data : NUM_BATCHES);

	auto batched_data = nn::BatchData(shuffled_data, num_batches);

	for (int epoch = 1; epoch <= epochs; ++epoch)
	{
		std::cout << "epoch " << epoch << "\n";
		for (int batch = 0; batch < batched_data.size(); batch += num_batches)
		{
			double batch_error = 0.f;

			nn::vec1d<double> X;
			nn::vec1d<double> y_true;
			for (int idx = 0; idx < num_batches; ++idx)
			{
				X = batched_data.at(idx).first;
				y_true = batched_data.at(idx).second;

				auto output = net->Forward(X, y_true);

				//Note: prediction is of the same shape as the target, assumed when passing the target
				auto predictions = output["prediction"];
				auto targets = output["target"];

				assert(predictions.size() == targets.size());

				std::cout << "\n\tprediction: ";
				for (auto idx = 0; idx < predictions.size(); ++idx) {
					std::cout << " " << predictions.at(idx) << " ";
				}

				std::cout << "\n\ttarget: ";
				for (auto idx = 0; idx < targets.size(); ++idx) {
					std::cout << " " << targets.at(idx) << " ";
				}
				std::cout << "\n";

				net->Backward(output);

				//batch_error += net->BackpropError();
				break;//DEBUG
			}

			std::cout << "batch error= " << batch_error / num_batches << "\n";
			//net->Update();//Nullify the weight and bias gradients, and delta errors
			break;//DEBUG
		}
	}

	std::cin.get();

	return 0;
}

/*
int main_v1(void)
{
	const nn::vec2d<double> inputs = { {0.0, 0.0}, {1.0, 0.0}, {0.0, 1.0}, {1.0, 1.0} };
	const nn::vec1d<double> targets = {1.0, 0.0, 0.0, 1.0};

	std::shared_ptr<nn::FNN> net = std::make_shared<nn::FNN>(nn::Topology({ 2, 10, 10, 1 }), 0.5);

	double ave_error = 0.0;
	for (int epoch = 0; epoch < 300; ++epoch)
	{
		double epoch_error = 0.0;
		for (int idx = 0; idx < inputs.size(); ++idx)
		{
			net->SetInputVector(inputs.at(idx));
			net->SetTargetVector({ targets.at(idx) });

			net->FeedForward();

			auto predictions = net->Outputs();
			auto targets = net->Targets();

			assert(predictions.size() == targets.size());

			std::cout << "Predictions: ";
			for (auto idx = 0; idx < predictions.size(); ++idx) {
				std::cout << " " << predictions.at(idx) << " ";
			}

			std::cout << "\nTargets: ";
			for (auto idx = 0; idx < targets.size(); ++idx) {
				std::cout << " " << targets.at(idx) << " ";
			}
			std::cout << "\n";

			net->Backprop();

			epoch_error = net->BackpropError();
		}

		std::cout << "Epoch error= " << epoch_error / inputs.size() << "\n";
		ave_error += epoch_error;
		ave_error /= epoch + 1;
		std::cout << "Average error= " << ave_error << "\n";
	}

	std::cin.get();

	return 0;
}
*/
