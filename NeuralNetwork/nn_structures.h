#ifndef NN_STRUCTURES_H__
#define NN_STRUCTURE_H__

#include <iostream>
#include <string>
#include <vector>
#include <type_traits>

namespace nn
{
	template <typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value> >
	using vec1d = std::vector<T>;

	template <typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value> >
	using vec2d = std::vector<vec1d<T> >;

	template <typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value> >
	using vec3d = std::vector<vec2d<T> >;

	void Print1d(const vec1d<double>& vec1d, const std::string& title);

	void Print2d(const vec2d<double>& vec2d, const std::string& title);

	void Print3d(const vec3d<double>& vec3d, const std::string& title);

	void Shape1d(const vec1d<double>& vec1d, const std::string& title);

	void Shape2d(const vec2d<double>& vec2d, const std::string& title);

	void Shape3d(const vec3d<double>& vec3d, const std::string& title);
}	//	namespace nn

#endif
